
The msgqueue Module for Drupal

Message-oriented middleware is a common means to enable communication between
heterogeneous systems. For example, if a java system wants to know about events
happening in Drupal, this messaging module is one way to provide such information.

The "msgqueue" module integrates Drupal with messaging system like Apache ActiveMQ.
This module accesses Drupal events by implementing hooks for users, nodes, and comments,
and is licensed under GPLv2. The hooks supply events when any insert, update, or delete
action takes place for any user, node or comment. An XML message about any such Drupal
event is sent to the messaging system. The module formats XML messages using minixml,
and puts them into a queue via the Stomp protocol of ActiveMQ.

More info at http://codeguild.com/presentations/DrupalMsgQueue.html ; the author can
be contacted at that website.
