<?php


/**
 * @file Send HTTP messages using the Stomp protocol, http://stomp.codehaus.org/Protocol
 * @author Larry Hamel, Codeguild.com
 */

/**
 * A StompMsg is a simple message object
 *
 */
class StompMsg {
    var $cmd;
    var $headers;
    var $txtmsg;

	/**
	 * constructor
	 */
    function StompMsg($cmd = NULL, $headers=NULL, $txtmsg=NULL) {
        $this->cmd = $cmd;
        $this->headers = $headers;
        $this->txtmsg = $txtmsg;
    }
}

/**
 * Stomp Connection
 *
 * Keeps connection so that command, headers and txtmsg can be transmitted for
 * any number of independent messages to the same destination.
 *
 */
class StompConn {

    var $socket;

	/**
	 * constructor connects to socket
	 */
    function StompConn($host, $port = 61613) {
        $this->socket = socket_create(AF_INET, SOCK_STREAM, 0) or die("Cannot create socket\n");
        $result = socket_connect($this->socket, $host, $port) or die("Cannot connect to server\n");
    }

	/**
	 * write a CONNECT message with credentials
	 * @return the result read from connection
	 */
    function login($userName="", $password="") {
        $this->_writeMsg( new StompMsg("CONNECT", array("login"=>$userName, "passcode"=> $password ) ) );
        return $this->readMsg();
    }

	/**
	 * send a message that should be directed to the queue indicated by the $dest param
	 * @param dest the queue or other named channel
	 * @param txtmsg the payload
	 * @param properties an optional array of headers
	 */
    function send($dest, $txtmsg, $properties=NULL) {
        $headers = array();
        if( isset($properties) ) {
            foreach ($properties as $name => $value) {
                $headers[$name] = $value;
            }
        }
        $headers["destination"] = $dest ;
        $this->_writeMsg( new StompMsg("SEND", $headers, $txtmsg) );
    }

	/**
	 * write out the given message to the socket
	 */
    function _writeMsg($stompMsg) {
        $data = $stompMsg->cmd . "\n";
        if( isset($stompMsg->headers) ) {
            foreach ($stompMsg->headers as $name => $value) {
                $data .= $name . ": " . $value . "\n";
            }
        }
        $data .= "\n";
        if( isset($stompMsg->txtmsg) ) {
            $data .= $stompMsg->txtmsg;
        }
        $l1 = strlen($data);
        $data .= "\x00\n";
        $l2 = strlen($data);

        socket_write($this->socket, $data, strlen($data)) or die("Cannot send stomp message to server\n");
    }

	/**
	 * read message from connection
	 */
    function readMsg() {
		$buffer = "";
        $in = socket_recv($this->socket, $buffer, 1, 0);

        if( $in == 0 ) {
            return NULL;
        }

        if( $in == false ) {
            return NULL;
        }

        $instring = NULL;

        while( ord($buffer) != 0  ) {

            $instring .= $buffer;
            $t = ord($buffer);

            $in = socket_recv($this->socket,$buffer,1,0);

            if( $in == 0 ) {
                return NULL;
            }

        }

        # get past the newline that follows the 0 delimitter
        $in = socket_recv($this->socket,$buffer,1,0);
        if( $in == 0 ) {
            return NULL;
        }
        if( ord($buffer) != 10 ) {
            return NULL;
        }

        list($header, $txtmsg) = explode("\n\n", $instring, 2);
        $header = explode("\n", $header);
        $allheaders = array();

        $cmd = NULL;
        foreach ($header as $oneheader) {
           if( isset($cmd) ) {
                list($name, $myval) = explode(':', $oneheader, 2);
                $allheaders[$name]=$myval;
           } else {
                $cmd = $oneheader;
           }
        }

        return new StompMsg($cmd, $allheaders, $txtmsg);
    }
}


